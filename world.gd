extends Spatial

onready var videoplayer = $Screens/Viewport/VideoPlayer
var zoom = 0.0

func _ready():
	get_tree().connect("files_dropped", self, "_on_file_dropped")

	var VR = ARVRServer.find_interface("OpenXR")
	if VR and VR.initialize():
		get_viewport().arvr = true
		get_viewport().hdr = false
		get_viewport().keep_3d_linear = false
		get_viewport().usage=Viewport.USAGE_3D_NO_EFFECTS

		OS.vsync_enabled = false
		Engine.iterations_per_second = 90

	load_file("/home/ruben/Videos/testSBS.mp4")

func load_file(file):
	var stream = videoplayer.stream
	stream = VideoStreamGDNative.new()
	var resource = load(file)
	
	if not "SBS" in file:
		print("Disabling SBS")
		$Screens/ScreenL.get_active_material(0).set_shader_param("side_by_side", false)
		$Screens/ScreenR.get_active_material(0).set_shader_param("side_by_side", false)

	videoplayer.set_stream(resource)
	videoplayer.play()
	print("Playing ", file)
	
func _on_file_dropped(files, screen):
	print(files)
	load_file(files[0])

var pos = 0
func _input(event):
	if event.is_action_pressed("ui_left"):
		if videoplayer.is_playing():
			pos = videoplayer.stream_position
			videoplayer.stream_position = 0 if pos < 5 else pos-5
		else:
			videoplayer.play()
			videoplayer.stream_position = pos
	if event.is_action_pressed("ui_right"):
			if videoplayer.is_playing():
				pos = videoplayer.stream_position
				videoplayer.stream_position = pos + 5
			else:
				videoplayer.play()
				videoplayer.stream_position = pos
	if event.is_action_pressed("ui_down"):
		if videoplayer.is_playing():
			pos = videoplayer.stream_position
			videoplayer.stream_position = 0 if pos < 20 else pos - 60
		else:
			videoplayer.play()
			videoplayer.stream_position = pos
	if event.is_action_pressed("ui_up"):
			if videoplayer.is_playing():
				pos = videoplayer.stream_position
				videoplayer.stream_position = pos + 60
			else:
				videoplayer.play()
				videoplayer.stream_position = pos
	if event.is_action_pressed("ui_select"):
			if videoplayer.is_playing():
				pos = videoplayer.stream_position
				videoplayer.stop()
			else:
				videoplayer.play()
				videoplayer.stream_position = pos
	if event.is_action_pressed("ui_page_up"):
		zoom += 0.1
		$Screens.translation[2] = -zoom
	if event.is_action_pressed("ui_page_down"):
		if zoom >= 0.2:
			zoom -= 0.1
			$Screens.translation[2] = -zoom
