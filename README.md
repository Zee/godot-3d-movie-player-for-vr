
# A stereoscopic 3D movie player for Godot-OpenXR, for half side-by-side or over-under video formats.

## Features:

* Drag-and-drop a video file onto the program window to play it.
* Press enter to reset the HMD position
* Press space to pause/play, arrow keys to seek through the video, and PgUp PgDown to move the screen back/forward.
* This program includes an advanced view mode in where the parallax of eye projection can be customized, as well as the inter-pupillar distance and the brightness of each eye screen. This is hidden by default and can be enabled by pressing backspace. Use WASD to set the parallax, keys Q and E to set the IPD, keys Z and X to set the brightness, and P to print current values.

## DISCLAIMER:

The advanced display mode is intended for the research of behavioral optometry. Do not use for therapeutic applications without the supervision of an optometrist. The author declines all responsibility for the use of this program as a therapeutic tool.

## Dependencies:

* Godot Videodecoder

    Adds support for playing any video format supported by ffmpeg. Instructions to integrate with the system's ffmpeg on GNU/Linux:

        git clone https://github.com/kidrigger/godot-videodecoder.git
        cd godot-videodecoder
        scons -c platform=x11
        scons platform=x11 prefix=/tmp
        ldd /tmp/x11/libgdnative_videodecoder.so

    Copy the libgdnative_videodecoder.so file into addons/godot-videodecoder/bin/x11/libgdnative_videodecoder.so

* Godot OpenXR

    Download using AssetLib or from https://github.com/GodotVR/godot_openxr/releases

